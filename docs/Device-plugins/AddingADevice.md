After loading plugins following the instructions [found here](https://bu-edf.gitlab.io/BUTool/Device-plugins/LoadingPlugins/), the device classes implemented in those plugins can be added to BUTool for use.

## BUTool launch
When device plugins are written, they must be registered with the [DeviceFactory](https://github.com/BU-Tools/BUTool/blob/develop/include/BUTool/DeviceFactory.hh). In the factory registration, device classes are given command line flags in order to simplify startup of BUTool if you want to use it with a specific device class. 

If you know the flag for a given device, and the [config file](https://bu-edf.gitlab.io/BUTool/Device-plugins/LoadingPlugins/#config-file) contains the proper `lib` entry pointing to the device's shared library file, then you can automatically bring up BUTool with the desired device added by calling:

```
BUTool.exe -<device_flag> [<device_args>]
```

For instance, to automatically load an ApolloSMDevice:

```
BUTool.exe -a /path/to/connections_file.xml
```

The existing device classes, along with their flags and arguments are:

* `APOLLOSM` : `-a` or `-ApolloSM`, XML connections file
* `GENERICIPBUS` : `-g` or `GenericIPBus`, XML connections file
* `IPMISOL` : `-s` or `-IPMISOL`, IP address

## In BUTool
To add a device from within BUTool, call: 

```
add_device <device_name> [<device_args>]
```

The existing device names are `APOLLOSM`, `GENERICIPBUS` and `IPMISOL`.

In order to add a device, its plugin must first be loaded (see ['Loading Plugins'](https://bu-edf.gitlab.io/BUTool/Device-plugins/LoadingPlugins/)).

## Config file
After [adding plugin libraries to the config file](https://bu-edf.gitlab.io/BUTool/Device-plugins/LoadingPlugins/#config-file), you can also specify a default device to add on BUTool launch by including the line 

```
DEFAULT_ARGS=<device_name> [<device_args>]
```

to the config file at `/etc/BUTool`.

For instance, on Apollo SM02 the default configurations file looks like:

```
DEFAULT_ARGS=ApolloSM /opt/address_table/connections_file.xml
lib=/opt/BUTool/lib/libBUTool_ApolloSMDevice.so
lib=/opt/BUTool/lib/libBUTool_GenericIPBusDevice.so
```

**NOTE:** despite showing up in BUTool with all-caps, the device name listed in the config file must be the same as the name chosen when registering the device with the DeviceFactory in the code. See the example config file above. 

When BUTool starts up, it first reads the config file to determine the default device and arguments, along with any additional device libraries to load. Next BUTool checks the `BUTOOL_AUTOLOAD_LIBRARY_LIST` environment variable to gather additional libraries to load. 