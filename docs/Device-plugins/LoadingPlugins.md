Device-specific plugin libraries for BUTool can be loaded in three different ways: from the command line, from setting an environment variable, and from a config file (located at `/etc/BUTool`). The functionality for loading plugins from the environment variable and the config file is implemented in the main [BUTool.cxx](https://github.com/BU-Tools/BUTool/blob/develop/src/tool/BUTool.cxx) file.

After loading a plugin library, the device class corresponding to that library will be registered with BUTool, and can be added manually with the `add_device` command.

## CLI
In order to load plugins from the command line, you need to launch BUTool with the following arguments:

```
BUTool.exe --cmd add_lib /path/to/plugin_library.so
```

The `--cmd` flag tells BUTool to run the `add_lib` command, which loads the specified plugin library and registers the associated device with BUTool.

## Environment variable
Plugin libraries can be automatically loaded to the installed BUTool executable by setting the `BUTOOL_AUTOLOAD_LIBRARY_LIST` environment variable with the absolute paths to every desired plugin library. 

For instance, on Apollo SM02, `BUTOOL_AUTOLOAD_LIBRARY_LIST` contains the default installation paths of all device libraries:

```
[cms@apollo02 ~]$ echo $BUTOOL_AUTOLOAD_LIBRARY_LIST
/opt/BUTool/lib/libBUTool_GenericIPBusDevice.so:/opt/BUTool/lib/libBUTool_GenericIPBusDevice.so:/opt/BUTool/lib/libBUTool_ApolloSMDevice.so:/opt/BUTool/lib/libBUTool_GenericIPBusDevice.so:/opt/BUTool/lib/libBUTool_ApolloSMDevice.so:/opt/BUTool/lib/libBUTool_IPMISOLDevice.so
```

Then, upon running BUTool the plugins are automatically loaded and the devices registered:

```
[cms@apollo02 ~]$ BUTool.exe
Registered device: GENERICIPBUS
Registered device: APOLLOSM
Registered device: IPMISOL
```

## Config file
Finally, plugins can be automatically loaded to BUTool through the use of a config file, which should be located at `/etc/BUTool`. The config file can contain the paths of plugin libraries as well as the name and arguments of the default device to be loaded (see ['Adding a Device'](https://bu-edf.gitlab.io/BUTool/Device-plugins/AddingADevice/#config-file)).

To specify a plugin library to load on BUTool startup, add the line

```
lib=/path/to/plugin_library.so
```

Multiple plugins can be loaded by specifying multiple `lib` arguments. 

On Apollo SM02, for example, the config file contains:

```
lib=/opt/BUTool/lib/libBUTool_ApolloSMDevice.so
lib=/opt/BUTool/lib/libBUTool_GenericIPBusDevice.so
```

