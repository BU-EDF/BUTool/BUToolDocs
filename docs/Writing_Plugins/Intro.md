# Overview
Though BUTool plugins are basically just a class that maps command strings to function calls via the CommandList class, best practices are to build two classes, a stand-alone class that talks to hardware and a device class that serves as the plugin. 
The reason for this is to allow BUTool and any other experiment software to access the hardware using the exact same code. 
This aids in feature synchronization and system debugging.

# Recommended structure

## HW class
This class needs nothing from BUTool (though it can be useful to use BUExceptions in it).
In here you add your fundamental register read/write calls and any code that does complicated operations on your HW.
This code should be used in DAQ, Slow Control, or monitoring software in the experiment.

## Device plugin class
This class bridges your HW class and BUTool.
It would be best to name this NameOfHWClass_device and for it to have an instance of your HW class as a private member. 
It must publicly inherit from CommandList\<DevicePluginClassName\> and can publicly inherit from RegisterHelper(or a derived class) to get existing read,write, and other useful register interaction commands. 

## Using RegisterHelper class

## Registration Macro (required!)
The registration macro used for registering devices to the DeviceFactory has the format:
```
RegisterDevice(ClassName,
			   ClassNickName,
			   ClassHelp,
			   CLIFlag,
			   CLIFullFlag,
			   CLIDescription);
```

* `ClassName`: Name of the C++ device class.
* `ClassNickName`: Nickname of the class. Used when [adding a device](https://bu-edf.gitlab.io/BUTool/Device-plugins/AddingADevice/#config-file) via the BUTool's config file.
* `ClassHelp`: Help string (usually containing proper arguments) displayed when device is improperly added.
* `CLIFlag`: Flag used to load device class automatically when launching BUTool. See ['Adding a Device'](https://bu-edf.gitlab.io/BUTool/Device-plugins/AddingADevice/#butool-launch).
* `CLIFullFlag`: Full version of `CLIFlag`
* `CLIDescription`: Description string (*add more detail*)

For example, the ApolloSMDevice class' [DeviceFactory registration](https://github.com/apollo-lhc/ApolloSM_plugin/blob/develop/include/ApolloSM_device/ApolloSM_device.hh#L81) looks like:

```
RegisterDevice(ApolloSMDevice,
			  "ApolloSM",
			  "file/SM_SN",
			  "a",
			  "ApolloSM",
			  "Connection file for creating an ApolloSM device"); 
```

