##Debugging help

### UIO_uHAL issues
You can see what UIO devices are being mapped in a UIOuHAL using BUTool plugin by running BUTool with the enviornment variable "UIOUHAL_DEBUG" set to 1.

Ex:

```
[root@apollo202 /]# UIOUHAL_DEBUG=1 BUTool.exe -a                                                                                                                         
Registered device: APOLLOSM
Registered device: GENERICIPBUS
Using .xml connection file...
Warning: Input is a connection file but no device entry specified, using default entry name: test.0
searching for /dev/uio_AXI_MON symlink
Added:
  dev#:     31
  addr:     0xB0070000
  uio name: "uio_AXI_MON"
  hw  name: "AXI_MON"
  size:     0x00004000
  map:      0xffff9fa23000
searching for /dev/uio_C2C1_AXILITE_FW symlink
Added:
  dev#:     14
  addr:     0xB0020000
  uio name: "uio_C2C1_AXILITE_FW"
  hw  name: "C2C1_AXILITE_FW"
  size:     0x00004000
  map:      0xffff9fa13000
```


### TEXT IO issues
TextIO debug mode can be turned on as follows
Ex:
```
[root@apollo202 /]# TEXTIO_DEBUG=1 BUTool.exe -a
Registered device: APOLLOSM
Registered device: GENERICIPBUS
Using .xml connection file...
Warning: Input is a connection file but no device entry specified, using default entry name: test.0
dynamic_cast RegisterHelper this -> BUTextIO succeeded
```
