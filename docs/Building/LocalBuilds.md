# Local Builds

This section provides information about locally building and running the `BUTool` software. The first sub-section below summarizes the pre-requisite packages that need to be installed, and the second sub-section summarizes the build instructions.

## Pre-requisites

### Common pre-req's
Here are the common pre-requisite packages for the `BUTool` software:

* boost-devel
* readline-devel
* ld
* boost-program-options
* boost-regex

### Ubuntu packages required for build
If building on Ubuntu, you can install the necessary packages before the build via the `apt` command:
```
  $ sudo apt install libboost-regex-dev \
      build-essential \
      zlib1g-dev \
      libreadline-dev \
      libboost-dev \
      libboost-program-options-dev \
      libncurses5-dev
```

## Building & Installing

### Build instructions
After checking out the BUTool source from the [GitHub repository](https://github.com/BU-Tools/BUTool), you can execute:
```Bash
 $ cp mk/Makefile.local ./Makefile
 $ make
 $ source env.sh
 $ # Go to the bin/ directory where BUTool.exe is located
 $ cd bin/BUTool
 $ ./BUTool.exe
```
Now `BUTool` can run in the local environment.

### Build flags: StatusDisplay Name Parser

`StatusDisplay` class of `BUTool` uses a name parser to read and process the `Row`, `Column` and `Table` parameters provided in the address table. About setting row and column names for a register in the address table, you can consult [here](https://bu-edf.gitlab.io/BUTool/AddressTable/Parameters/#row-and-column-names) for more information.

Currently, there are two versions of such name parsers used in `BUTool::StatusDisplay` class:

**Older version:** Will treat a single underscore character as a spcecial character. If this underscore is followed by a digit, it will indicate the position within the register name to take as row/column name. If it is not followed by a digit, it will be treated as a literal underscore. For the detailed explanation of syntax, please consult [here](https://bu-edf.gitlab.io/BUTool/AddressTable/Parameters/#row-and-column-names). As an example, if we had a register with name `A.B.C.D` and parameter `Row=_3`, the row name for this register would be `C`. 

**Newer version:** Will treat double and triple underscore characters as special. Double underscore replaces the single underscore functionality in the older version, while triple underscore implies a reverse count. Using the same example from before, if we had a register with name `A.B.C.D`, the following is true:

- If this register specifies `Row=__3`, the row name would be `C`.
- If this register specifies `Row=___3`, the row name would be `B`.

By default, when the `BUTool::StatusDisplay` class parses the XML address table, it will use the **older version** of the name parser. This behavior can be updated by using a compile-time flag however, called `BUTOOL_SD_USE_NEW_PARSER`. To use the new name parser, you can add `BUTOOL_SD_USE_NEW_PARSER=true` to the `make` command while building `BUTool`.

### Build flags: Linter

In the latest `v2.0` version, `BUTool` has a linting feature added to the build pipeline, which uses the `clang-tidy` command. Before the compilation step, this will throw an exception if a non-recoverable issue is found in the source code (e.g. a missing include file).

However, the linting skip can be skipped by setting the `LINTER` environment variable accordingly. If you want to skip the linting step during the build, you can execute:

```bash
LINTER=: make
```

The command above will set the `LINTER` to a `NULL` value, and hence `make` will skip linting.

### Installing
After you build BUTool, you can install it to the system by using:
```bash
  $ make install INSTALL_PATH=/path/to/install/to
```

