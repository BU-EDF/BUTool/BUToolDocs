# Status Display Parameters

As specified in the previous section, each node can take a `Parameters` attribute. The parameters specified here will be used by the `StatusDisplay` class of the BUTool, to infer which status table we want to register the node to, row and column names for this node, and so forth. A full set of possible parameters are shown in the table below:

|Parameter keys	| Required|	Description|
|---|---|---|
|Status    | Required | Status uHAL parameter. Integer display priority 1=always display|
|Show	   | Optional | Show uHAL parameter. "nz" to suppress zero values, "z" to suppress non-zero values (see note below)|
|Table     | Required | Table uHAL parameter. Name of table for status display|
|Row	   | Required | Row uHAL parameter. Name of row in table for status display|
|Column    | Required | Column uHAL parameter. Name of column in table for status display|
|Format    | Optional | Format uHAL parameter.  |

All keys are concatenated into the address table attribute `parameters` which is processed as a semicolon-delimited `keyword=value` list by uHAL. No special characters are permitted in these columns (only letters, numbers and `"_"`). For an item to appear in the status displays, the `Status`, `Table`, `Row` and `Column` **must be specified**. P_Table, P_Row and P_Column are sorted alphabetically. Leading digits and `"_"` characters should be stripped before display so that e.g. "0_" can be used to force a sort order.

## Details on Parameters

This sub-section has information on how the parameter keys mention above can be specified, and their interpretation by the `StatusDisplay` class of `BUTool`. 

### Status
The `Status` key sets the display priority. 0 is used to mark configuration registers which may be optionally displayed or not according to user preference. Positive integers 1-9 indicate various levels of detail for status values, with value 1 indicating the highest priority items which should always be displayed. On the other hand, value 9 indicates the lowest priority items, which will only be displayed when a call to `status` command is made with status level 9.

In general, if `Status=N` is specified for a node, it will **only** be displayed in `BUTool` calls to `status M` command, where `M` is greater or equal to `N`. In `BUTool`, `M` defaults to 1, so a call to `status` will only display the most high-priority registers (i.e. `Status=1`). 

### Row and Column Names
In the `Row` and `Column` fields, the sequence `_N`, where `N` is a digit between 1-9, results in substituting the corresponding field from the delimited id, e.g. if a field is `_2` and the register ID is `STATUS.AMC01.ERRORS`, then the resulting value would be `AMC01`. It is also possible to specify multiple special sequences: Following the same example, if `Row=_2_3` is specified, the resulting row name would be `AMC01 ERRORS`. Note that if a `_N` sequence is specified such that `N` is larger than the register value length (in this example, it is 3), `BUTool` will stop execution and will throw a `BAD_MARKUP_NAME` exception. 

It is possible to specify literal underscores in the row/column names as well. This can be done in two ways:

* Use a double underscore (i.e. `'__'`). For example, if `Row=ROW__3` is specified, the resulting row name would be `ROW_3`.
* Follow the underscore with a non-digit character. For example, if `Row=USER_INFO` is specified, the resulting row name would be `USER_INFO`, since the character following the underscore is not a digit.

To wrap it up, if we're given a register with an ID of `STATUS.AMC01.ERRORS`, here are some examples that display the parsing technique:

| Row/Column Value | Resulting Name |
|---|---|
| _3       | ERRORS | 
| _2_3     | AMC01 ERRORS | 
| VALUE_3  | VALUE ERRORS | 
| VALUE__3 | VALUE_3 | 
| VALUE_ERRORS | VALUE_ERRORS | 
| _4 | Invalid markup, `BAD_MARKUP_NAME` error | 

**Note:** Please also note that, the same parsing technique described here will be applied to the `Table` parameter as well (i.e. the table name).

### Show
The `Show` parameter has several possible settings:

| Value | Interpretation |
|---|---|
| nz   | Display this value only if it is non-zero |
| z    | Display this value only if it is zero |
| nzr  | Display the entire row in the corresponding table only if this value is non-zero |

### Format
This parameter will specify how the value is to be formatted and printed out in it's respective table. Below is a list of possible formats accepted by `BUTool`:

| Format | Interpretation |
|---|---|
| "x"/"X" | Print number as hex |
| "d"     | Print as a signed decimal number |
| "u"     | Print as a unsigned decimal number |
| "t"     | Print bare enumerations. Enums are set by following "t" with `_number1_ENUMname1_number2_ENUMname2` etc. |
| "T"     | Print enumerations along with the numerical value. Enums are defined the same way as "t" |
| "M"/"m" | Convert from integer to floating point using the equation below |
| "linear11" | `linear11` float format |
| "fp16"  | Using ARM `fp16` format |
| "IP"    | Convert 32-bit number into an IP address |

The equation to convert integer to a floating point for `Format="m"/"M"` is as follows:

y = (sign) \* (M_n/M_d) \* x + (sign) \* (b_n/b_d)

where the `Format` is specified as: `Format=m_sign_Mn_Md_sign_bn_bd`. `sign=0` denotes a negative sign, whereas all other values indicate a positive sign.

## Examples
Below, you can find some example address table entries that have several different use cases.

```xml
<!-- Unsigned integer with status level 1 -->
<node id="COUNT"    mask="0x000000FF" permission="rw" parameters="Table=ZYNQ_OS;Row=_3;Column=_4;Format=u;Status=1"/>

<!-- This value will be ONLY shown if it is non-zero -->
<node id="MINS"     mask="0x0000FF00" permission="rw" parameters="Table=ZYNQ_OS;Row=_3;Column=_4;Format=u;Status=1;Show=nz"/>

<!-- The original unsigned int value will be divided by 100 to get the CPU_LOAD -->
<node id="CPU_LOAD"  mask="0x0000FFFF" permission="rw" parameters="Table=ZYNQ_OS;Row=_3;Column=Percent;Format=m_1_1_100_1_0_1;Status=1"/>

<!-- Enumeration: A value of 0 will display INACTIVE, and 1 will display ACTIVE -->
<node id="ACTIVE"    address="0x0" mask="0x00000100" permission="r" parameters="Table=CM_UART_MON;Column=_2;Row=MON_STATE;Status=1;Format=t_0_INACTIVE_1_ACTIVE" />
```

## 64-bit Registers
By default, values from a single register can be **at most** 32-bits. However, for 64-bit register values, `BUTool` has the functionality to merge the values from two separate registers, and display a 64-bit integer.

To achieve this, two registers need to be specified: One register ID will end with `_LO` suffix, while the other will end with `_HI`. The register IDs **must be identical otherwise.** `BUTool` will handle the merging of such register values into a single 64-bit value, and display only that.

## AXI FW unblocking

Adding `Unblock=name` to the parameters list will cause a action write to that register if a call to axiunblock is called with the matching name. 

## Default value

Adding `default=val` on a rw register will cause that register to reset/power-up-reset to that value instead of '0'.
