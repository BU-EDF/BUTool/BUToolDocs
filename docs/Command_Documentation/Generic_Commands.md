# Generic Commands

These are commands which should be available in every instance of BUTool, regardless of what plugins are installed.  Additional features which are plugin-based are documented here where appropriate. Command aliases are specified in parentheses. 

## add_dev_ofile

```add_dev_ofile filename <device#> <stream#>```

Send device output to a file (in addition to standard output).
If `device#` is not specified, affects all devices.  Use ```list```
to list connected devices. `stream#` specifies the stream to write to
the output file. If this is not specified, the `Level::INFO` stream will
be written. The user can specify the following integer values to indicate different
streams to write:

* 0: `Level::INFO`
* 1: `Level::DEBUG`
* 2: `Level::ERROR`

Note that any other `stream#` value will raise a `BAD_ARGS` exception. 

## add_device

```add_device <type> <args>```

Add a new device and make it active.  ```<args>``` are specific to the device type.

### Generic IPBus Devices

```add_device GENERICIPBUS <connection_file> [<device ID>]```

Add a generic IPBus device.  Specify the filename of the connection file,
and optionally the device ID.  If the device ID is not specified,
currently defaults (not very helpfully) to ```test.0```.

### ApolloSM Device

```add_device APOLLOSM <connection_file> [<device ID>]```

Add an ApolloSM device. Usage is identical to that of a GenericIPBus device.

### IPMISOL Device

```add_device IPMISOL <IP_address>```

Add an IPMISOL device. Specify the IP address of the device with which to open an IPMI SOL connection.

## add_lib

```add_lib /path/to/libBUTool_<Device>.so```

Add a device class library. Specify the path to the relevant shared object library. Once added, the library will be used when a new device of that type is added via the `add_device` command. Current device library names are as follows:

- `libBUTool_GenericIPBusDevice.so`
- `libBUTool_ApolloSMDevice.so`
- `libBUTool_IPMISOLDevice.so`

## echo

```echo foo```

Echo text to screen.

## exit

Exits the BUTool CLI application. 

## help (h)

```help [<command>]```

List commands. Specify a `command` for additional info and usage.

## include (load)

Read a script file.

## list 

Lists all devices currently connected.

## quit (q)

Exits the BUTool CLI application. 

## select

```select <device number>```

Switch the active device.

## sleep

```sleep <integer number of seconds>```

Delay in seconds.

## verbose

```verbose <integer level>```

Change the print level for exceptions (9 == all).
